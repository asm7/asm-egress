## ASM Egress Tutorial

[DEVSITE LINK](https://cloud.devsite.corp.google.com/service-mesh/docs/security/egress-gateway-gke-tutorial)

### Steps

1.  Create WORKDIR.

    ```bash
    mkdir -p asm-egress-tutorial && cd asm-egress-tutorial && export WORKDIR=`pwd`
    ```

1.  Define variables.

    ```bash
    export ORG_ID=<YOUR ORG_ID>
    export ORG_NAME=<YOUR ORG NAME>
    export ADMIN_USER=<YOUR ADMIN USER EMAIL>
    export BILLING_ACCOUNT=<YOUR BILLING ACCOUNT>
    export FOLDER_NAME=asm-egress-tutorial
    export PROJECT_NAME=asm-egress
    export REGION=us-west2
    export ZONE=us-west2-a
    # Shared VPC
    export VPC=vpc-network
    export SUBNET_1_REGION=us-west2
    export SUBNET_1_NAME=subnet-1-${SUBNET_1_REGION}
    export SUBNET_1_RANGE=10.0.0.0/24
    export SUBNET_1_POD_NAME=${SUBNET_1_NAME}-pods
    export SUBNET_1_POD_RANGE=10.1.0.0/16
    export SUBNET_1_SVC_1_NAME=${SUBNET_1_NAME}-svc-1
    export SUBNET_1_SVC_1_RANGE=10.2.0.0/20
    export PRIVATE_GOOGLE_ACCESS_IPS=199.36.153.8/30
    # GCP SA
    export SA_APPLICATION_NODEPOOL=sa-application-nodes
    export SA_GATEWAY_NODEPOOL=sa-gateway-nodes
    # GKE
    export GKE=gke-egress
    export MASTER_IP_CIDR=10.5.0.0/28
    export MY_IP=$(curl -s ifconfig.me)
    export KUBECONFIG=$WORKDIR/asm-kubeconfig
    export TEAM_X_GCP_SA=sa-test-app-team-x
    export TEAM_X_KSA=team-x-ksa
    ```

1.  Create a folder and a project in your org.

    ```bash
    gcloud alpha resource-manager folders create \
    --display-name=${FOLDER_NAME} \
    --organization=${ORG_ID}

    export FOLDER_ID=$(gcloud resource-manager folders list --organization=${ORG_ID} | grep ${FOLDER_NAME} | awk '{print $3}')
    [ -z "$FOLDER_ID" ] && echo "FOLDER_ID is not exported" || echo "FOLDER_ID is $FOLDER_ID"
    echo -e "export FOLDER_ID=$FOLDER_ID" >> $WORKDIR/vars.sh

    gcloud projects create ${PROJECT_NAME}-${FOLDER_ID} \
    --folder ${FOLDER_ID}
    gcloud beta billing projects link ${PROJECT_NAME}-${FOLDER_ID} \
    --billing-account ${BILLING_ACCOUNT}

    echo -e "export PROJECT_ID=${PROJECT_NAME}-${FOLDER_ID}" >> $WORKDIR/vars.sh
    source $WORKDIR/vars.sh

    gcloud --project=${PROJECT_ID} services enable \
    dns.googleapis.com \
    container.googleapis.com \
    compute.googleapis.com \
    monitoring.googleapis.com \
    logging.googleapis.com \
    cloudtrace.googleapis.com \
    meshca.googleapis.com \
    meshtelemetry.googleapis.com \
    meshconfig.googleapis.com \
    iamcredentials.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    cloudresourcemanager.googleapis.com \
    stackdriver.googleapis.com
    ```

1.  Set default project, region and zone.

    ```bash
    gcloud config set project ${PROJECT_ID}
    gcloud config set compute/region ${REGION}
    gcloud config set compute/zone ${ZONE}
    ```

1.  Create VPC.

    ```bash
    gcloud --project=${PROJECT_ID} compute networks create ${VPC} \
    --subnet-mode custom

    gcloud --project=${PROJECT_ID} compute networks subnets create ${SUBNET_1_NAME} \
    --network ${VPC} \
    --range ${SUBNET_1_RANGE} \
    --region ${SUBNET_1_REGION} \
    --secondary-range ${SUBNET_1_POD_NAME}=${SUBNET_1_POD_RANGE},${SUBNET_1_SVC_1_NAME}=${SUBNET_1_SVC_1_RANGE} \
    --enable-private-ip-google-access
    ```

1.  Create Cloud Nat gateway in the region.

    ```bash
    gcloud compute addresses create ${SUBNET_1_REGION}-nat-ip \
    --project=${PROJECT_ID} --region=${SUBNET_1_REGION}
    export NAT_REGION_1_IP_ADDR=$(gcloud compute addresses describe ${SUBNET_1_REGION}-nat-ip --project=${PROJECT_ID} --region=${SUBNET_1_REGION} --format='value(address)')
    export NAT_REGION_1_IP_NAME=$(gcloud compute addresses describe ${SUBNET_1_REGION}-nat-ip --project=${PROJECT_ID} --region=${SUBNET_1_REGION} --format='value(name)')

    [ -z "$NAT_REGION_1_IP_ADDR" ] && echo "NAT_REGION_1_IP_ADDR is not exported" || echo "NAT_REGION_1_IP_ADDR is $NAT_REGION_1_IP_ADDR"
    echo -e "export NAT_REGION_1_IP_ADDR=$NAT_REGION_1_IP_ADDR" >> $WORKDIR/vars.sh

    [ -z "$NAT_REGION_1_IP_NAME" ] && echo "NAT_REGION_1_IP_NAME is not exported" || echo "NAT_REGION_1_IP_NAME is $NAT_REGION_1_IP_NAME"
    echo -e "export NAT_REGION_1_IP_NAME=$NAT_REGION_1_IP_NAME" >> $WORKDIR/vars.sh

    gcloud compute routers create rtr-${SUBNET_1_REGION} \
    --project ${PROJECT_ID} \
    --network=${VPC} \
    --region ${SUBNET_1_REGION}

    gcloud compute routers nats create nat-gw-${SUBNET_1_REGION} \
    --project ${PROJECT_ID} \
    --router=rtr-${SUBNET_1_REGION} \
    --region ${SUBNET_1_REGION} \
    --nat-external-ip-pool=${NAT_REGION_1_IP_NAME} \
    --nat-all-subnet-ip-ranges \
    --enable-logging
    ```

1.  Create two service accounts for use by the two GKE node pools. A separate service account is assigned to each node pool so that you can apply VPC firewall rules to specific nodes.

    ```bash
    gcloud --project=${PROJECT_ID} iam service-accounts create sa-application-nodes \
    --description="SA for application nodes" \
    --display-name="${SA_APPLICATION_NODEPOOL}"

    gcloud --project=${PROJECT_ID} iam service-accounts create sa-gateway-nodes \
    --description="SA for gateway nodes" \
    --display-name="${SA_GATEWAY_NODEPOOL}"
    ```

1.  Add a minimal set of IAM roles to the application and gateway service accounts. These roles are required for logging, monitoring, and pulling private container images from Container Registry.

    ```bash
    project_roles=(
        roles/logging.logWriter
        roles/monitoring.metricWriter
        roles/monitoring.viewer
        roles/storage.objectViewer
        )
    for role in "${project_roles[@]}"
    do
        gcloud projects add-iam-policy-binding ${PROJECT_ID} \
            --member="serviceAccount:${SA_APPLICATION_NODEPOOL}@${PROJECT_ID}.iam.gserviceaccount.com" \
            --role="$role"
        gcloud projects add-iam-policy-binding ${PROJECT_ID} \
            --member="serviceAccount:${SA_GATEWAY_NODEPOOL}@${PROJECT_ID}.iam.gserviceaccount.com" \
            --role="$role"
    done
    ```

1.  Apply a firewall rule to the VPC network so that, by default, all egress traffic is denied. Specific connectivity is required for the cluster to function and for gateway nodes to be able to reach destinations outside of the VPC. A minimal set of specific firewall rules overrides the default `deny-all` rule to allow the necessary connectivity.

    ```bash
    # Default deny all EGRESS
    gcloud --project=${PROJECT_ID} compute firewall-rules create global-deny-egress-all \
    --action DENY \
    --direction EGRESS \
    --rules all \
    --destination-ranges 0.0.0.0/0 \
    --network ${VPC} \
    --priority 65535 \
    --description "Default rule to deny all egress from the network."

    # Rule for gateway nodes ONLY to EGRESS to the internet
    gcloud --project=${PROJECT_ID} compute firewall-rules create gateway-allow-egress-web \
    --action ALLOW \
    --direction EGRESS \
    --rules tcp:80,tcp:443 \
    --target-service-accounts ${SA_GATEWAY_NODEPOOL}@${PROJECT_ID}.iam.gserviceaccount.com \
    --network ${VPC} \
    --priority 1000 \
    --description "Allow the nodes running the egress gateways to connect to the web"

    # Allow nodes to reach GKE controlplane
    gcloud --project=${PROJECT_ID} compute firewall-rules create allow-egress-to-api-server \
    --action ALLOW \
    --direction EGRESS \
    --rules tcp:443,tcp:10250 \
    --destination-ranges 10.5.0.0/28 \
    --network ${VPC} \
    --priority 1000 \
    --description "Allow nodes to reach the Kubernetes API server."

    # Anthos Service Mesh uses webhooks when injecting sidecar proxies into workloads
    gcloud --project=${PROJECT_ID} compute firewall-rules create allow-ingress-api-server-to-webhook \
    --action ALLOW \
    --direction INGRESS \
    --rules tcp:15017 \
    --source-ranges ${MASTER_IP_CIDR} \
    --network ${VPC} \
    --priority 1000 \
    --description "Allow the API server to call the webhooks exposed by istiod discovery"

    # Egress connectivity between pods and services running on the cluster
    gcloud --project=${PROJECT_ID} compute firewall-rules create allow-egress-pods-and-services \
    --action ALLOW \
    --direction EGRESS \
    --rules all \
    --destination-ranges ${SUBNET_1_POD_RANGE},${SUBNET_1_SVC_1_RANGE} \
    --network ${VPC} \
    --priority 1000 \
    --description "Allow pods and services on nodes to reach each other"

    # Calico provides NetworkPolicy API functionality for GKE. Allow connectivity for Calico within the subnet
    gcloud --project=${PROJECT_ID} compute firewall-rules create allow-egress-calico \
    --action ALLOW \
    --direction EGRESS \
    --rules tcp:5473 \
    --destination-ranges ${SUBNET_1_RANGE} \
    --network ${VPC} \
    --priority 1000 \
    --description "Allow Calico Typha within the subnet"

    # Kubelet read-only port is needed for GKE to read node metrics. Allow access to it within the subnet
    gcloud --project=${PROJECT_ID} compute firewall-rules create allow-egress-kubelet-readonly \
    --action ALLOW \
    --direction EGRESS \
    --rules tcp:10255 \
    --destination-ranges ${SUBNET_1_RANGE} \
    --network ${VPC} \
    --priority 1000 \
    --description "Allow access to the kubelet read-only port within the subnet"

    # Allow access to the reserved sets of IP addresses used by Private Google Access for serving Google APIs, Container Registry, and other services
    gcloud --project=${PROJECT_ID} compute firewall-rules create allow-egress-gcp-apis \
    --action ALLOW \
    --direction EGRESS \
    --rules tcp \
    --destination-ranges ${PRIVATE_GOOGLE_ACCESS_IPS} \
    --network ${VPC} \
    --priority 1000 \
    --description "Allow access to the VIPs used by Google Cloud APIs (Private Google Access)"

    # Allow the Google Cloud health checker service to access pods running in the cluster
    gcloud --project=${PROJECT_ID} compute firewall-rules create allow-ingress-gcp-health-checker \
    --action ALLOW \
    --direction INGRESS \
    --rules tcp:80,tcp:443 \
    --source-ranges 130.211.0.0/22,35.191.0.0/16,35.191.0.0/16,209.85.152.0/22,209.85.204.0/22 \
    --network ${VPC} \
    --priority 1000 \
    --description "Allow workloads to respond to Google Cloud health checks"
    ```

1.  Configure Private Google Access for Google Cloud APIs.

    ```bash
    # Create a private DNS zone, a 'CNAME', and 'A' records so that nodes and workloads can connect to Google APIs and services using Private Google Access and the 'private.googleapis.com' hostname
    gcloud --project=${PROJECT_ID} dns managed-zones create private-google-apis \
    --description "Private DNS zone for Google APIs" \
    --dns-name googleapis.com \
    --visibility private \
    --networks ${VPC}

    gcloud --project=${PROJECT_ID} dns record-sets transaction start --zone private-google-apis

    gcloud --project=${PROJECT_ID} dns record-sets transaction add private.googleapis.com. \
        --name "*.googleapis.com" \
        --ttl 300 \
        --type CNAME \
        --zone private-google-apis

    gcloud --project=${PROJECT_ID} dns record-sets transaction add "199.36.153.8" \
    "199.36.153.9" "199.36.153.10" "199.36.153.11" \
        --name private.googleapis.com \
        --ttl 300 \
        --type A \
        --zone private-google-apis

    gcloud --project=${PROJECT_ID} dns record-sets transaction execute --zone private-google-apis
    ```

1.  Create a private DNS zone, a 'CNAME' and an 'A' record so that nodes can connect to Container Registry using Private Google Access and the 'gcr.io' hostname

    ```bash
    gcloud --project=${PROJECT_ID} dns managed-zones create private-gcr-io \
    --description "private zone for Container Registry" \
    --dns-name gcr.io \
    --visibility private \
    --networks ${VPC}

    gcloud --project=${PROJECT_ID} dns record-sets transaction start --zone private-gcr-io

    gcloud --project=${PROJECT_ID} dns record-sets transaction add gcr.io. \
        --name "*.gcr.io" \
        --ttl 300 \
        --type CNAME \
        --zone private-gcr-io

    gcloud --project=${PROJECT_ID} dns record-sets transaction add "199.36.153.8" "199.36.153.9" "199.36.153.10" "199.36.153.11" \
        --name gcr.io \
        --ttl 300 \
        --type A \
        --zone private-gcr-io

    gcloud --project=${PROJECT_ID} dns record-sets transaction execute --zone private-gcr-io
    ```

1.  Create a private GKE cluster.

    ```bash
    gcloud --project=${PROJECT_ID} container clusters create ${GKE} \
    --enable-ip-alias \
    --enable-private-nodes \
    --release-channel "regular" \
    --no-enable-basic-auth \
    --no-issue-client-certificate \
    --enable-master-authorized-networks \
    --master-authorized-networks ${MY_IP//\"}/32 \
    --master-ipv4-cidr ${MASTER_IP_CIDR} \
    --enable-network-policy \
    --service-account "${SA_APPLICATION_NODEPOOL}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --machine-type "e2-standard-4" \
    --num-nodes "4" \
    --network "${VPC}" \
    --subnetwork "${SUBNET_1_NAME}" \
    --cluster-secondary-range-name "${SUBNET_1_POD_NAME}" \
    --services-secondary-range-name "${SUBNET_1_SVC_1_NAME}" \
    --workload-pool "${PROJECT_ID}.svc.id.goog" \
    --zone ${ZONE}
    ```

1.  Create a node pool called gateway. This node pool is where the egress gateway is deployed. The `dedicated=gateway:NoSchedule` [taint](https://cloud.devsite.corp.google.com/kubernetes-engine/docs/how-to/node-taints) is added to every node in the gateway node pool.

    ```bash
    gcloud --project=${PROJECT_ID} container node-pools create "gateway" \
    --cluster "${GKE}" \
    --zone ${ZONE} \
    --machine-type "e2-standard-4" \
    --node-taints dedicated=gateway:NoSchedule \
    --service-account "${SA_GATEWAY_NODEPOOL}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --num-nodes "1"
    ```

1.  Access to GKE cluster.

    ```bash
    export KUBECONFIG=$WORKDIR/asm-kubeconfig
    gcloud --project=${PROJECT_ID} container clusters get-credentials ${GKE} --zone ${ZONE}
    ```

1.  Verigy the gateway nodes have correct taints.

    ```bash
    kubectl get nodes -l cloud.google.com/gke-nodepool=gateway -o yaml \
    -o=custom-columns='name:metadata.name,taints:spec.taints[?(@.key=="dedicated")]'
    ```

1.  Create namespaces for ASM.

    ```bash
    kubectl create ns istio-system
    kubectl create ns istio-egress

    # Label namespaces, for NetworkPolicies and istio-injection=disabled to prevent istioctl error msgs
    kubectl label ns istio-egress istio=egress istio-injection=disabled
    kubectl label ns istio-system istio=system
    kubectl label ns kube-system kube-system=true
    ```

1.  Create a manifest file to customize the Anthos Service Mesh installation using the [Istio OperatorAPI](https://istio.io/latest/docs/reference/config/istio.operator.v1alpha1/).

    ```bash
    cat << 'EOF' > $WORKDIR/asm-custom-install.yaml
    apiVersion: install.istio.io/v1alpha1
    kind: IstioOperator
    metadata:
    name: "egress-gateway"
    spec:
    meshConfig:
      accessLogFile: "/dev/stdout"
    components:
      egressGateways:
        - name: "istio-egressgateway"
          enabled: true
          namespace: "istio-egress"
          label:
            istio: "egress"
          k8s:
            tolerations:
            - key: "dedicated"
              operator: "Equal"
              value: "gateway"
            nodeSelector:
              cloud.google.com/gke-nodepool: "gateway"
    EOF
    ```

1.  Download and install ASM with the custom overlay to create egress gateways.

    ```bash
    curl -O https://storage.googleapis.com/csm-artifacts/asm/install_asm
    chmod +x install_asm

    ./install_asm \
    --mode install \
    --project_id ${PROJECT_ID} \
    --cluster_name ${GKE} \
    --cluster_location ${ZONE} \
    --custom_overlay $WORKDIR/asm-custom-install.yaml \
    --output_dir ./ \
    --enable_all
    ```

1.  Save the `istioctl` command in a variable.

    ```bash
    export ISTIOCTL=$(find "$(pwd -P)" -name istioctl)
    echo "ISTIOCTL=\"${ISTIOCTL}\"" >> $WORKDIR/vars.sh
    ```

1.  Examine the nodeSelector and tolerations for the egress gateway pods

    ```bash
    kubectl -n istio-egress get pod -l app=istio-egressgateway \
    -o=custom-columns='name:metadata.name,nodeSelector:spec.nodeSelector,\
    tolerations:spec.tolerations[?(@.key=="dedicated")]'
    ```

1.  Make sure that STRICT mutual TLS is enabled. Apply a default PeerAuthentication policy for the mesh in the istio-system namespace.

    ```bash
    cat <<EOF > $WORKDIR/auth-policy-strict-mtls.yaml
    apiVersion: "security.istio.io/v1beta1"
    kind: "PeerAuthentication"
    metadata:
      name: "default"
      namespace: "istio-system"
    spec:
      mtls:
        mode: STRICT
    EOF

    kubectl apply -f $WORKDIR/auth-policy-strict-mtls.yaml
    ```

1.  Create two namespaces simulating two teams for testing. Label the namespaces for NetworkPolicies later as well as label them for istio sidecare injection based on theversion of ASM installed.

    ```bash
    kubectl create namespace team-x
    kubectl create namespace team-y

    kubectl label namespace team-x team=x
    kubectl label namespace team-y team=y

    export REVISION_LABEL=$(kubectl get pod -n istio-system -l app=istiod \
    -o jsonpath='{.items[0].metadata.labels.istio\.io/rev}')
    echo $REVISION_LABEL

    kubectl label ns team-x istio.io/rev=${REVISION_LABEL}
    kubectl label ns team-y istio.io/rev=${REVISION_LABEL}
    ```

1.  Create test deployment for `team-x`.

        ```bash

    cat << EOF > $WORKDIR/test.yaml
    apiVersion: v1
    kind: ServiceAccount
    metadata:
    name: ${TEAM_X_KSA}

---

apiVersion: v1
kind: Service
metadata:
name: test
labels:
app: test
spec:
ports:

- port: 80
  name: http
  selector:
  app: test

---

apiVersion: apps/v1
kind: Deployment
metadata:
name: test
spec:
replicas: 1
selector:
matchLabels:
app: test
template:
metadata:
labels:
app: test
spec:
serviceAccountName: ${TEAM_X_KSA}
containers: - name: test
image: gcr.io/google.com/cloudsdktool/cloud-sdk:slim
command: ["/bin/sleep", "infinity"]
imagePullPolicy: IfNotPresent
EOF

    kubectl -n team-x create -f $WORKDIR/test.yaml
    kubectl -n team-x wait --for=condition=available deploy test
    ```

1.  Verify that it is not possible to make an HTTP request from the test container to an external site.

    ```bash
    kubectl -n team-x exec -it \
    $(kubectl -n team-x get pod -l app=test -o jsonpath={.items..metadata.name}) \
    -c test -- curl -v http://example.com
    ```

1.  Inspect the outbound clusters configured in the Envoy sidecar proxy for the test pod by running the istioctl proxy-config command.

    ```bash
    ${ISTIOCTL} pc c $(kubectl -n team-x get pod -l app=test \
    -o jsonpath={.items..metadata.name}).team-x --direction outbound
    ```

1.  Restrict the proxy configuration to egress routes that have been explicitly defined with service entries in the istio-egress and team-x namespaces. Apply a Sidecar resource to the team-x namespace.

    ```bash
    cat <<EOF > $WORKDIR/sidecar-egress-and-team-se-only.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: Sidecar
    metadata:
      name: default
      namespace: team-x
    spec:
      outboundTrafficPolicy:
        mode: REGISTRY_ONLY
      egress:
      - hosts:
        - 'istio-egress/*'
        - 'team-x/*'
    EOF

    kubectl apply -f $WORKDIR/sidecar-egress-and-team-se-only.yaml
    ```

1.  Re-examine the clusters on the sidecar proxy. You should see only the clusters for egress and the test pod itself.

    ```bash
    ${ISTIOCTL} pc c $(kubectl -n team-x get pod -l app=test \
    -o jsonpath={.items..metadata.name}).team-x --direction outbound
    ```

1.  Create a gateway for egress to accept any host.

    ```bash
    cat <<EOF > $WORKDIR/egress-gateway.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: Gateway
    metadata:
      name: egress-gateway
      namespace: istio-egress
    spec:
      selector:
        istio: egress
      servers:
      - port:
          number: 80
          name: https
          protocol: HTTPS
        hosts:
          - '*'
        tls:
          mode: ISTIO_MUTUAL
    EOF

    kubectl apply -f $WORKDIR/egress-gateway.yaml
    ```

1.  Create a DestinationRule for the egress gateway with mutual TLS for authentication and encryption. Use a single shared destination rule for all external hosts.

    ```bash
    cat <<EOF > $WORKDIR/destrule-mtls.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: DestinationRule
    metadata:
      name: target-egress-gateway
      namespace: istio-egress
    spec:
      host: istio-egressgateway.istio-egress.svc.cluster.local
      subsets:
      - name: target-egress-gateway-mTLS
        trafficPolicy:
          loadBalancer:
            simple: ROUND_ROBIN
          tls:
            mode: ISTIO_MUTUAL
    EOF

    kubectl apply -f $WORKDIR/destrule-mtls.yaml
    ```

1.  Create a ServiceEntry in the istio-egress namespace to explicitly register example.com in the mesh's service registry for the team-x namespace.

    ```bash
    cat <<EOF > $WORKDIR/se-example-com.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: ServiceEntry
    metadata:
      name: example-com-ext
      namespace: istio-egress
    spec:
      hosts:
      - example.com
      ports:
      - number: 80
        name: http
        protocol: HTTP
      - number: 443
        name: tls
        protocol: TLS
      resolution: DNS
      location: MESH_EXTERNAL
      exportTo:
      - 'team-x'
      - 'istio-egress'
    EOF

    kubectl apply -f $WORKDIR/se-example-com.yaml
    ```

The `exportTo` property controls which namespaces can use the service entry. Network administrators use service entries to centrally control the set of external hosts available to each namespace. Configure Kubernetes RBAC permissions so that only network administrators can directly create and modify service entries.

1.  Create a VirtualService to route traffic to example.com through the egress gateway. There are two match conditions: the first condition directs traffic to the egress gateway, and the second directs traffic from the egress gateway to the destination host.

    ```bash
    cat <<EOF > $WORKDIR/vs-example-com.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: VirtualService
    metadata:
      name: example-com-through-egress-gateway
      namespace: istio-egress
    spec:
      hosts:
      - example.com
      gateways:
      - istio-egress/egress-gateway
      - mesh
      http:
      - match:
        - gateways:
          - mesh
          port: 80
        route:
        - destination:
            host: istio-egressgateway.istio-egress.svc.cluster.local
            subset: target-egress-gateway-mTLS
            port:
              number: 80
          weight: 100
      - match:
        - gateways:
          - istio-egress/egress-gateway
          port: 80
        route:
        - destination:
            host: example.com
            port:
              number: 80
          weight: 100
      exportTo:
      - 'istio-egress'
      - 'team-x'
    EOF

    kubectl apply -f $WORKDIR/vs-example-com.yaml
    ```

1.  Run istioctl analyze to ensure there are no validation issues.

    ```bash
    ${ISTIOCTL} analyze -n istio-egress
    ```

1.  Test connectivity to example.com.

    ```bash
    for i in {1..4}
    do
        kubectl -n team-x exec -it $(kubectl -n team-x get pod -l app=test \
            -o jsonpath={.items..metadata.name}) -c test -- \
        curl -s -o /dev/null -w "%{http_code}\n" http://example.com
    done
    ```

1.  Verify egress gateway is indeed being used to send traffic to example.com.

    ```bash
    # Sidecar pod
    kubectl -n team-x logs -f $(kubectl -n team-x get pod -l app=test \
    -o jsonpath={.items..metadata.name}) istio-proxy

    # Egress gateway pod
    kubectl -n istio-egress logs -f $(kubectl -n istio-egress get pod -l istio=egress \
    -o jsonpath="{.items[0].metadata.name}") istio-proxy
    ```

1.  Configure routing for a second external host to learn how different external connectivity can be configured for different teams. Create a test deployment and a sidecar rule for team-y.

    ```bash
    kubectl -n team-y create -f $WORKDIR/test.yaml
    kubectl -n team-y wait --for=condition=available deploy test

    cat <<EOF > $WORKDIR/sidecar-team-y.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: Sidecar
    metadata:
      name: default
      namespace: team-y
    spec:
      outboundTrafficPolicy:
        mode: REGISTRY_ONLY
      egress:
      - hosts:
        - 'istio-egress/*'
        - 'team-y/*'
    EOF

    kubectl apply -f $WORKDIR/sidecar-team-y.yaml
    ```

1.  Create a ServiceEntry and VirtualService for a second destination (httpbin.org) for both team-x and team-y. Analyze the config.

    ```bash
    cat <<EOF > $WORKDIR/se-httobin-org.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: ServiceEntry
    metadata:
      name: httpbin-org-ext
      namespace: istio-egress
    spec:
      hosts:
      - httpbin.org
      ports:
      - number: 80
        name: http
        protocol: HTTP
      - number: 443
        name: tls
        protocol: TLS
      resolution: DNS
      location: MESH_EXTERNAL
      exportTo:
      - 'istio-egress'
      - 'team-x'
      - 'team-y'
    EOF

    kubectl apply -f $WORKDIR/se-httobin-org.yaml

    cat <<EOF > $WORKDIR/vs-httobin-org.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: VirtualService
    metadata:
      name: httpbin-org-through-egress-gateway
      namespace: istio-egress
    spec:
      hosts:
      - httpbin.org
      gateways:
      - istio-egress/egress-gateway
      - mesh
      http:
      - match:
        - gateways:
          - mesh
          port: 80
        route:
        - destination:
            host: istio-egressgateway.istio-egress.svc.cluster.local
            subset: target-egress-gateway-mTLS
            port:
              number: 80
          weight: 100
      - match:
        - gateways:
          - istio-egress/egress-gateway
          port: 80
        route:
        - destination:
            host: httpbin.org
            port:
              number: 80
          weight: 100
      exportTo:
      - 'istio-egress'
      - 'team-x'
      - 'team-y'
    EOF

    kubectl apply -f $WORKDIR/vs-httobin-org.yaml

    ${ISTIOCTL} analyze -n istio-egress
    ```

1.  Make requests to httpbin.org from both team-x and team-y. These should be successful. Make requests from team-y to example.com and that should fail.

    ```bash
    # Team-y to httpbin.org - 200 OK
    kubectl -n team-y exec -it $(kubectl -n team-y get pod -l app=test -o \
    jsonpath={.items..metadata.name}) -c test -- curl -I http://httpbin.org

    # Team-x to httpbin.org - 200 OK
    kubectl -n team-x exec -it $(kubectl -n team-x get pod -l app=test \
    -o jsonpath={.items..metadata.name}) -c test -- curl -I http://httpbin.org

    # Team-y to example.com - FAIL 502 Bad Gateway
    kubectl -n team-y exec -it $(kubectl -n team-y get pod -l app=test \
    -o jsonpath={.items..metadata.name}) -c test -- curl -I http://example.com
    ```

1.  Create an AuthorizationPolicy so that applications in the team-x namespace can connect to example.com but not to other external hosts when sending requests using port 80. The corresponding targetPort on the egress gateway pods is 8080.

    ```bash
    cat <<EOF > $WORKDIR/authpolicy-team-x-example-com.yaml
    apiVersion: security.istio.io/v1beta1
    kind: AuthorizationPolicy
    metadata:
      name: egress-team-x-to-example-com
      namespace: istio-egress
    spec:
      rules:
        - from:
          - source:
              namespaces:
              - 'team-x'
          to:
          - operation:
              hosts:
                - 'example.com'
          when:
          - key: destination.port
            values: ["8080"]
    EOF

    kubectl apply -f $WORKDIR/authpolicy-team-x-example-com.yaml
    ```

1.  Hit example.com from team-x test pod, this should work. Then hit httpbin.org, you should get a `RBAC: access denied 403`. Wait a few moments for the auth policy to kick in.

    ```bash
    # team-x to example.com - 200 OK
    kubectl -n team-x exec -it $(kubectl -n team-x get pod -l app=test \
    -o jsonpath={.items..metadata.name}) -c test -- curl -I http://example.com

    # team-x to httpbin.org - RBAC: acess denied 403
    kubectl -n team-x exec -it $(kubectl -n team-x get pod -l app=test \
    -o jsonpath={.items..metadata.name}) -c test -- curl -s -w " %{http_code}\n" \
    http://httpbin.org
    ```

1.  Authorization policies provide rich control over which traffic is allowed or denied. Apply the following authorization policy to allow the test app in the team-y namespace to make requests to httpbin.org by using one particular URL path when sending requests using port 80. The corresponding targetPort on the egress gateway pods is 8080.

    ```bash
    cat <<EOF > $WORKDIR/authpolicy-team-y-httpbin-418.yaml
    apiVersion: security.istio.io/v1beta1
    kind: AuthorizationPolicy
    metadata:
      name: egress-team-y-to-httpbin-teapot
      namespace: istio-egress
    spec:
      rules:
        - from:
          - source:
              namespaces:
              - 'team-y'
          to:
          - operation:
              hosts:
              - httpbin.org
              paths: ['/status/418']
          when:
          - key: destination.port
            values: ["8080"]
    EOF

    kubectl apply -f $WORKDIR/authpolicy-team-y-httpbin-418.yaml
    ```

1.  Wait a few moments. Test from team-y to httpbin.org/ and then to httpbin.org/status/418. First one should fail with RBAC: access desnied and the second one should pass.

    ```bash
    # Team-y to httpbin.org/ - RBAC: access denied 403
    kubectl -n team-y exec -it $(kubectl -n team-y get pod -l app=test \
    -o jsonpath={.items..metadata.name}) -c test -- curl -s -w " %{http_code}\n" \
    http://httpbin.org

    # Team-y to httpbin.org/status/418 - 200 OK
    kubectl -n team-y exec -it $(kubectl -n team-y get pod -l app=test \
    -o jsonpath={.items..metadata.name}) -c test -- curl http://httpbin.org/status/418
    ```

1.  Configure TLS origination at egress gateway. This means that you can send a web request at port 80 and when it reaches thye egress gateway, the request egress sends to, for example, example.com would be sent using TLS on port 443.

    ```bash
    cat <<EOF > $WORKDIR/dr-egress-tls-origination.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: DestinationRule
    metadata:
      name: example-com-originate-tls
      namespace: istio-egress
    spec:
      host: example.com
      subsets:
        - name: example-com-originate-TLS
          trafficPolicy:
            loadBalancer:
              simple: ROUND_ROBIN
            portLevelSettings:
            - port:
                number: 443
              tls:
                mode: SIMPLE
                sni: example.com
    EOF

    kubectl apply -f $WORKDIR/dr-egress-tls-origination.yaml
    ```

1.  Update the virtual service for example.com so that requests to port 80 on the gateway are 'upgraded' to TLS on port 443 when they are sent to the destination host.

    ```bash
    cat <<EOF > $WORKDIR/vs-example-com-tls-origin.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: VirtualService
    metadata:
      name: example-com-through-egress-gateway
      namespace: istio-egress
    spec:
      hosts:
      - example.com
      gateways:
      - mesh
      - istio-egress/egress-gateway
      http:
      - match:
        - gateways:
          - mesh
          port: 80
        route:
        - destination:
            host: istio-egressgateway.istio-egress.svc.cluster.local
            subset: target-egress-gateway-mTLS
            port:
              number: 80
      - match:
        - gateways:
          - istio-egress/egress-gateway
          port: 80
        route:
        - destination:
            host: example.com
            port:
              number: 443
            subset: example-com-originate-TLS
          weight: 100
    EOF

    kubectl apply -f $WORKDIR/vs-example-com-tls-origin.yaml
    ```

1.  Hit example.com and then check egress gateway logs to verify TLS origination is working.

    ```bash
    for i in {1..4}
    do
        kubectl -n team-x exec -it $(kubectl -n team-x get pod -l app=test \
            -o jsonpath={.items..metadata.name}) -c test -- curl -I http://example.com
    done

    # Get egress logs to verify TLS origination
    kubectl -n istio-egress logs -f $(kubectl -n istio-egress get pod -l istio=egress \
    -o jsonpath="    {.items[0].metadata.name}") istio-proxy
    ```

1.  Configure TLS pass through. This means that applications that are already sending TLS traffic will simply pass through the egress gateway and egress gateway will not unencrypt them. Egress gateway simply treats these as TCP sessions and hence you cannot read any HTTP related metadat to make authorization decisions. Avoid this if you want a more secure authz implementation. Update the Gateway, DestinationRule and VirtualService as follows.

    ```bash
    cat <<EOF > $WORKDIR/gateway-egress-tls-pass-through.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: Gateway
    metadata:
      name: egress-gateway
      namespace: istio-egress
    spec:
      selector:
        istio: egress
      servers:
      - port:
          number: 80
          name: https
          protocol: HTTPS
        hosts:
          - '*'
        tls:
          mode: ISTIO_MUTUAL
      - port:
          number: 443
          name: tls
          protocol: TLS
        hosts:
        - '*'
        tls:
          mode: PASSTHROUGH
    EOF

    kubectl apply -f $WORKDIR/gateway-egress-tls-pass-through.yaml

    cat <<EOF > $WORKDIR/destrule-egress-tls-pass-through.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: DestinationRule
    metadata:
      name: target-egress-gateway
      namespace: istio-egress
    spec:
      host: istio-egressgateway.istio-egress.svc.cluster.local
      subsets:
      - name: target-egress-gateway-mTLS
        trafficPolicy:
          loadBalancer:
            simple: ROUND_ROBIN
          portLevelSettings:
          - port:
              number: 80
            tls:
              mode: ISTIO_MUTUAL
      - name: target-egress-gateway-TLS-passthrough
    EOF

    kubectl apply -f $WORKDIR/destrule-egress-tls-pass-through.yaml

    cat <<EOF > $WORKDIR/vs-egress-example-com-tls-pass-through.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: VirtualService
    metadata:
      name: example-com-through-egress-gateway
      namespace: istio-egress
    spec:
      hosts:
      - example.com
      gateways:
      - mesh
      - istio-egress/egress-gateway
      http:
      - match:
        - gateways:
          - mesh
          port: 80
        route:
        - destination:
            host: istio-egressgateway.istio-egress.svc.cluster.local
            subset: target-egress-gateway-mTLS
            port:
              number: 80
      - match:
        - gateways:
          - istio-egress/egress-gateway
          port: 80
        route:
        - destination:
            host: example.com
            port:
              number: 443
            subset: example-com-originate-TLS
          weight: 100
      tls:
      - match:
        - gateways:
          - mesh
          port: 443
          sniHosts:
          - example.com
        route:
        - destination:
            host: istio-egressgateway.istio-egress.svc.cluster.local
            subset: target-egress-gateway-TLS-passthrough
            port:
              number: 443
      - match:
        - gateways:
          - istio-egress/egress-gateway
          port: 443
          sniHosts:
          - example.com
        route:
        - destination:
            host: example.com
            port:
              number: 443
          weight: 100
      exportTo:
      - 'istio-egress'
      - 'team-x'
    EOF

    kubectl apply -f $WORKDIR/vs-egress-example-com-tls-pass-through.yaml

    cat <<EOF > $WORKDIR/vs-egress-httpbin-org-tls-pass-through.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: VirtualService
    metadata:
      name: httpbin-org-through-egress-gateway
      namespace: istio-egress
    spec:
      hosts:
      - httpbin.org
      gateways:
      - istio-egress/egress-gateway
      - mesh
      http:
      - match:
        - gateways:
          - mesh
          port: 80
        route:
        - destination:
            host: istio-egressgateway.istio-egress.svc.cluster.local
            subset: target-egress-gateway-mTLS
            port:
              number: 80
          weight: 100
      - match:
        - gateways:
          - istio-egress/egress-gateway
          port: 80
        route:
        - destination:
            host: httpbin.org
            port:
              number: 80
          weight: 100
      tls:
      - match:
        - gateways:
          - mesh
          port: 443
          sniHosts:
          - httpbin.org
        route:
        - destination:
            host: istio-egressgateway.istio-egress.svc.cluster.local
            subset: target-egress-gateway-TLS-passthrough
            port:
              number: 443
      - match:
        - gateways:
          - istio-egress/egress-gateway
          port: 443
          sniHosts:
          - httpbin.org
        route:
        - destination:
            host: httpbin.org
            port:
              number: 443
          weight: 100
      exportTo:
      - 'istio-egress'
      - 'team-x'
      - 'team-y'
    EOF

    kubectl apply -f $WORKDIR/vs-egress-httpbin-org-tls-pass-through.yaml
    ```

1.  Add an authorization policy that accepts any kind of traffic sent to port 443 of the egress gateway service. The corresponding targetPort on the gateway pods is 8443.

    ```bash
    cat <<EOF > $WORKDIR/authpolicy-egress-tls-passthrough.yaml
    apiVersion: security.istio.io/v1beta1
    kind: AuthorizationPolicy
    metadata:
      name: egress-all-443
      namespace: istio-egress
    spec:
      rules:
        - when:
          - key: destination.port
            values: ["8443"]
    EOF

    kubectl apply -f $WORKDIR/authpolicy-egress-tls-passthrough.yaml
    ```

1.  Make a plain HTTP and several HTTPS requests from team-s and view the egress logs.

    ```bash
    # Plain HTTP to example.com
    kubectl -n team-x exec -it $(kubectl -n team-x get pod -l app=test \
    -o jsonpath={.items..metadata.name}) -c test -- curl -I http://example.com

    # HTTPS to example.com
    for i in {1..4}
    do
        kubectl -n team-x exec -it $(kubectl -n team-x get pod -l app=test \
            -o jsonpath={.items..metadata.name}) -c test -- curl -s -o /dev/null \
            -w "%{http_code}\n" \
            https://example.com
    done

    # Egress Pod logs - No HTTP info included in logs for HTTPS passthrough requests, treated as TCP
    kubectl -n istio-egress logs -f $(kubectl -n istio-egress get pod -l istio=egress \
    -o jsonpath="{.items[0].metadata.name}") istio-proxy
    ```

There are many scenarios in which an application can bypass a sidecar proxy. You can use Kubernetes NetworkPolicy to additionally specify which connections workloads are allowed to make. After a single network policy is applied, all connections that aren't specifically allowed are denied. This tutorial only considers egress connections and egress selectors for network policies. If you control ingress with network policies on your own clusters, then you must create ingress policies to correspond to your egress policies. For example, if you allow egress from workloads in the team-x namespace to the team-y namespace, you must also allow ingress to the team-y namespace from the team-x namespace.

1.  Allow workloads and proxies deployed in the team-x namespace to connect to istiod and the egress gateway. Allow workloads and proxies to query DNS and to IPs that serve Google APIs (including Mesh CA), to GKE metadata server, within team-x namespace and optionally to team-y as well.

    ```bash
    cat <<EOF > $WORKDIR/netpolicy-team-x-egress-to-istiod.yaml
    apiVersion: networking.k8s.io/v1
    kind: NetworkPolicy
    metadata:
      name: allow-egress-to-control-plane
      namespace: team-x
    spec:
      podSelector: {}
      policyTypes:
        - Egress
      egress:
      - to:
        - namespaceSelector:
            matchLabels:
              istio: system
          podSelector:
            matchLabels:
              istio: istiod
        - namespaceSelector:
            matchLabels:
              istio: egress
          podSelector:
            matchLabels:
              istio: egress
    EOF

    kubectl apply -f $WORKDIR/netpolicy-team-x-egress-to-istiod.yaml

    cat <<EOF > $WORKDIR/netpolicy-team-x-dns.yaml
    apiVersion: networking.k8s.io/v1
    kind: NetworkPolicy
    metadata:
      name: allow-egress-to-dns
      namespace: team-x
    spec:
      podSelector: {}
      policyTypes:
        - Egress
      egress:
      - to:
        - namespaceSelector:
            matchLabels:
              kube-system: "true"
        ports:
        - port: 53
          protocol: UDP
        - port: 53
          protocol: TCP
    EOF

    kubectl apply -f $WORKDIR/netpolicy-team-x-dns.yaml

    cat <<EOF > $WORKDIR/netpolicy-team-x-google-apis.yaml
    apiVersion: networking.k8s.io/v1
    kind: NetworkPolicy
    metadata:
      name: allow-egress-to-google-apis
      namespace: team-x
    spec:
      podSelector: {}
      policyTypes:
        - Egress
      egress:
      - to:
        - ipBlock:
            cidr: 199.36.153.4/30
        - ipBlock:
            cidr: 199.36.153.8/30
    EOF

    kubectl apply -f $WORKDIR/netpolicy-team-x-google-apis.yaml

    cat <<EOF > $WORKDIR/netpolicy-team-x-gke-metadata.yaml
    apiVersion: networking.k8s.io/v1
    kind: NetworkPolicy
    metadata:
      name: allow-egress-to-metadata-server
      namespace: team-x
    spec:
      podSelector: {}
      policyTypes:
        - Egress
      egress:
      - to: # For GKE data plane v2
        - ipBlock:
            cidr: 169.254.169.254/32
      - to: # For GKE data plane v1
        - ipBlock:
            cidr: 127.0.0.1/32
        ports:
        - protocol: TCP
          port: 988
    EOF

    kubectl apply -f $WORKDIR/netpolicy-team-x-gke-metadata.yaml

    cat <<EOF > $WORKDIR/netpolicy-team-x-intra.yaml
    apiVersion: networking.k8s.io/v1
    kind: NetworkPolicy
    metadata:
      name: allow-egress-to-same-namespace
      namespace: team-x
    spec:
      podSelector: {}
      ingress:
        - from:
          - podSelector: {}
      egress:
        - to:
          - podSelector: {}
    EOF

    kubectl apply -f $WORKDIR/netpolicy-team-x-intra.yaml

    cat <<EOF > $WORKDIR/netpolicy-team-x-to-team-y.yaml
    apiVersion: networking.k8s.io/v1
    kind: NetworkPolicy
    metadata:
      name: allow-egress-to-team-y
      namespace: team-x
    spec:
      podSelector: {}
      policyTypes:
        - Egress
      egress:
      - to:
        - namespaceSelector:
            matchLabels:
              team: 'y'
    EOF

    kubectl apply -f $WORKDIR/netpolicy-team-x-to-team-y.yaml
    ```

1.  Connections between sidecar proxies persist. Existing connections are not closed when you apply a new network policy. Restart the workloads in the team-x namespace to make sure existing connections are closed. And verify you can still make request to example.com.

    ```bash
    kubectl -n team-x rollout restart deployment
    kubectl -n team-x wait --for=condition=available deploy test

    kubectl -n team-x exec -it $(kubectl -n team-x get pod -l app=test \
    -o jsonpath={.items..metadata.name}) -c test -- curl -I http://example.com
    ```

Google's APIs and services are exposed using external IP addresses. When pods with VPC-native alias IP addresses make connections to Google APIs by using Private Google Access, the traffic never leaves Google's network.

When you set up the infrastructure for this tutorial, you enabled Private Google Access for the subnet used by GKE pods. To allow access to the IP addresses used by Private Google Access, you created a route, a VPC firewall rule, and a private DNS zone. This configuration lets pods reach Google APIs directly without sending traffic through the egress gateway. You can control which APIs are available to specific Kubernetes service accounts (and hence namespaces) by using Workload Identity and IAM. Istio authorization doesn't take effect because the egress gateway is not handling connections to the Google APIs.

1.  Set up IAM permissions for GKE Pods with WorkloadIdentity to access Google APIs.

    ```bash
    gcloud --project=${PROJECT_ID} iam service-accounts create ${TEAM_X_GCP_SA}

    gcloud --project=${PROJECT_ID}  iam service-accounts add-iam-policy-binding \
    --role roles/iam.workloadIdentityUser \
     --member "serviceAccount:${PROJECT_ID}.svc.id.goog[team-x/${TEAM_X_KSA}]" \
    ${TEAM_X_GCP_SA}@${PROJECT_ID}.iam.gserviceaccount.com

    cat <<EOF > $WORKDIR/ksa-team-x-with-workloadidentity.yaml
    apiVersion: v1
    kind: ServiceAccount
    metadata:
      annotations:
        iam.gke.io/gcp-service-account: ${TEAM_X_GCP_SA}@${PROJECT_ID}.iam.gserviceaccount.com
      name: ${TEAM_X_KSA}
      namespace: team-x
    EOF

    kubectl apply -f $WORKDIR/ksa-team-x-with-workloadidentity.yaml
    ```

1.  The test application pod must be able to access the Google metadata server (running as a DaemonSet) to obtain temporary credentials for calling Google APIs. Create a service entry for the GKE metadata server, as well as private.googleapis.com and storage.googleapis.com.

    ```bash
    cat <<EOF > $WORKDIR/se-gke-metadata.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: ServiceEntry
    metadata:
      name: metadata-google-internal
      namespace: istio-egress
    spec:
      hosts:
      - metadata.google.internal
      ports:
      - number: 80
        name: http
        protocol: HTTP
      - number: 443
        name: tls
        protocol: TLS
      resolution: DNS
      location: MESH_EXTERNAL
      exportTo:
      - 'istio-egress'
      - 'team-x'
    EOF

    kubectl apply -f $WORKDIR/se-gke-metadata.yaml

    cat <<EOF > $WORKDIR/se-private-and-storage-apis.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: ServiceEntry
    metadata:
      name: private-googleapis-com
      namespace: istio-egress
    spec:
      hosts:
      - private.googleapis.com
      - storage.googleapis.com
      ports:
      - number: 80
        name: http
        protocol: HTTP
      - number: 443
        name: tls
        protocol: TLS
      resolution: DNS
      location: MESH_EXTERNAL
      exportTo:
      - 'istio-egress'
      - 'team-x'
    EOF

    kubectl apply -f $WORKDIR/se-private-and-storage-apis.yaml
    ```

1.  Verify that the Kubernetes service account is correctly configured to act as the Google service account. You should see the Google SA.

    ```bash
    kubectl -n team-x exec -it $(kubectl -n team-x get pod -l app=test \
    -o jsonpath={.items..metadata.name}) -c test -- gcloud auth list
    ```

1.  Create a etst file in GCS, grant the TEAM_X_GCP_SA permission to view files in GCS and verify that the test Pod can access the test file.

    ```bash
    echo "Hello, World" > $WORKDIR/hello
    gsutil mb gs://${PROJECT_ID}-bucket
    gsutil cp $WORKDIR/hello gs://${PROJECT_ID}-bucket/

    gsutil iam ch \
    serviceAccount:${TEAM_X_GCP_SA}@${PROJECT_ID}.iam.gserviceaccount.com:objectViewer \
        gs://${PROJECT_ID}-bucket/

    kubectl -n team-x exec -it \
    $(kubectl -n team-x get pod -l app=test -o jsonpath={.items..metadata.name}) \
    -c test \
    -- gsutil cat gs://${PROJECT_ID}-bucket/hello
    ```

## Cleanup

1.  Delete the project and folder.

    ```bash
    gcloud projects delete "${PROJECT_ID}" --quiet
    gcloud resource-manager folders delete ${FOLDER_ID}
    ```
